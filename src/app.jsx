/* General */
var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');

/* CSS */
var css = {};
css.normalize = require('./styles/normalize.css');
css.appStyle = require('./styles/stylesheet.css')

/* SoundCloud Audio Variables */
var SoundCloudAudio = require('soundcloud-audio');
var scClientId = '04d6c07e99ecb852eaf1e639395c7fb7';
var scPlayer = new SoundCloudAudio(scClientId);
//The format must be set up like this to use for the render method prior to the audio player init
var scPlaylist = {
	tracks : [
		{
			user: {
				username: undefined
			},
			title : undefined,
			permalink_url: undefined,
			artwork_url: undefined
		}
	]
};
var scRegex = /https:\/\/api.soundcloud.com/; //Regex used to parse out the track stream url

/* Giphy Variables*/
var giphyApiKey= 'dc6zaTOxFJmzC';
var giphyRating = 'pg-13';
var fallbackVideoUrl = 'https://media.giphy.com/media/Bpqp2E59wkaOY/giphy.mp4';
var rotationTimer = 2243 * 2; //107bpm average for 80's pop * 4 so it should line up to beats
var rotator;

/* Configuration Variables */
var initialWaitTimeToHidePanel = 5000;
var gifTransitionAnimTime = 550; //Match this to the CSS animation time to ensure maximum smoothness
var mouseWaitTimeToHidePanel = 2200;
var mouseHidePanelTimer;
var KEY_CONST = {
	SPACE: 32,
	ESC: 27,
	LEFT_ARROW: 37,
	RIGHT_ARROW: 39
};


var App = React.createClass({
	getInitialState: function(){
		return {

			playerInit: false,
			// playlistUrl: 'https://soundcloud.com/redonesan/sets/synthwave', [copied over, left off at 200]
			playlistUrl: 'https://soundcloud.com/user-67255322/sets/80sfy',
			currentlyPlaying: false,
			currentTrackNum: 0,
			currentlyMuted: false,

			mobileDevice: false,
			mobileHasAudioStarted: false,

			showPanel: true,
			showSecretConfig: false,
			overrideInactivityPause: true,

			gif1Url: undefined,
			gif1Meta: undefined,
			gif1Img: undefined,
			gif2Url: undefined,
			gif2Meta: undefined,
			gif2Img: undefined,
			rotatorState: 0,
			gif1Visible: true,
			gifCurrentlyPlaying: true,

			gifSearchTags: [
				'80s',
				'1980',
				'80s art',
				'80s animation',
				'80s tv',
				'80s food',
				'80s movie',
				'80s music',
				'80s miami',
				'80s fashion',
				'80s scifi',
				'80s anime',
				'80s video games',
				'80s video',
				'vhs',
				'synthwave',
				'tron',
				'grid',
				'nes',
				'8bit',
				'pixel',
				'neon',
				'geometry',
				'kidmograph'
			]
		}
	},

	componentDidMount: function(){
		var self = this;

		//Device specific init
		if(!this.isMobileDevice()){
			//Desktop
			this.bindKeyPresses();
			this.bindMouseEvents();	
			this.bindGifsFallback();
			this.bindGifsPauseForInactiveWindow();
			//Hide panel after 3 seconds
			setTimeout(function(){
				self.hidePanel();
			}, initialWaitTimeToHidePanel);
		}
		else {
			//Mobile
			this.bindTouchEvents();
			this.setState({
				mobileDevice: true
			});
		}

		//Regular Init
		self.loadGif(2);
		this.createAudioPlayer();
		this.rotateGifs();

	},

	render: function(){
		var self = this;

		//Control panel data
		var paneltoggle = this.state.showPanel ? '' : 'hide';
		var playpausetoggle = this.state.currentlyPlaying ? 'fa-pause' : 'fa-play';
		var mutetoggle = this.state.currentlyMuted ? 'fa-volume-off' : 'fa-volume-up';

		//Secret panel data
		var secretconfigtoggle = this.state.showSecretConfig ? '' : 'hide';
		var searchterms = this.state.gifSearchTags.join(', ');
		var playlist = this.state.playlistUrl;

		//Track label/title etc data
		var trackArtist = scPlaylist.tracks[this.state.currentTrackNum].user.username || '';
		var trackTitle = scPlaylist.tracks[this.state.currentTrackNum].title || '';
		var trackUrl = scPlaylist.tracks[this.state.currentTrackNum].permalink_url || '';
		var trackCover = scPlaylist.tracks[this.state.currentTrackNum].artwork_url || '';

		//Pause state
		var gifPaused = (this.state.currentlyPlaying && !this.state.gifCurrentlyPlaying) ? '' : 'hide';
		var mobilePaused;

		//Gif rotating state
		var gifActive = (this.state.gif1Visible === true) ? 'active' : 'hide';
		var gif1;
		var gif2;

		//Mobile device GIF or HTML5 Video swap
		if(this.state.mobileDevice){
			gif1 = <img src={this.state.gif1Img} />;
			gif2 = <img src={this.state.gif2Img} />;

			//One time init of mobile audio. User must click to start HTML5 audio for session.
			if(!this.state.mobileHasAudioStarted) {
				mobilePaused = <div id="mobile-tap-to-start" onClick={this.mobileFirstPlayStart}><div id="mobile-tap-to-start-content">Tap to start audio</div></div>
			}
			else {
				mobilePaused = '';
			}
		}
		else {
			gif1 = <video src={this.state.gif1Url} autoPlay="true" loop="true"></video>;
			gif2 = <video src={this.state.gif2Url} autoPlay="true" loop="true"></video>
		}

		return (
			<div id="container">
				<div id="panel" className="control-panel">				
					<div id="panel-content" className={paneltoggle}>

						<div id="logo">
							<div className="scanlines"></div>
							<img src="assets/logo.png" />
						</div>

						<div id="track-info">
							<div className="scanlines"></div>
							<a href={trackUrl} target="_blank">
								<span className="track-cover"><img src={trackCover} /></span>
								<span className="track-title">{trackTitle}</span>
								<span className="track-artist">{trackArtist}</span>
							</a>
						</div>


						<div id="control">

							<div className="control-button" onClick={this.toggleMute}>
								<div className="scanlines"></div>
								<i className={'fa ' + mutetoggle}></i>
								<span>Toggle Mute</span>
							</div>

							<div className="control-button" onClick={this.toggleGlobalPlayPause}>
								<div className="scanlines"></div>
								<i className={'fa ' + playpausetoggle}></i>
								<span>Play / Pause</span>
							</div>

							<div className="control-button" onClick={this.playNextAudioTrack}>
								<div className="scanlines"></div>
								<i className="fa fa-fast-forward"></i>
								<span>Fast Forward</span>
							</div>

							<div className="control-button" onClick={this.toggleFullscreen}>
								<div className="scanlines"></div>
								<i className="fa fa-arrows-alt"></i>
								<span>Full Screen</span>
							</div>
						</div>


						<div id="credits">
							<div className="scanlines"></div>

							<div className="credit-header">
								Powered by
							</div>
							<div className="credit-icons">
								<a href="http://www.giphy.com" target="_blank">
									<img src="assets/giphy.png" alt="Giphy" />
								</a>
								<a href="http://www.soundcloud.com" target="_blank">
									<img src="assets/soundcloud.png" alt="SoundCloud" />
								</a>
							</div>
							<div className="credit-creator">
								Created by
								<br />
								<a href="http://www.digitalbloc.com" target="_blank">Art Sangurai</a>
							</div>
							<div className="credit-copyright">Copyright 2016</div>
						</div>
					</div>
				</div>


				<div id="player-gif-paused" className={gifPaused} >
					<div id="player-gif-pause-content">
						[GIFs Paused]
						<br />
						<br />
						Click here to make this the active window and continue GIFs
					</div>
				</div>

				{mobilePaused}

				<div className={'player-gif-container ' + gifActive } id="player-gif-1">
					{gif1}
				</div>

				<div className="player-gif-container" id="player-gif-2">
					{gif2}
				</div>

				<div id="secret-config-button" onClick={this.toggleSecretConfig}>
				</div>

				<div id="secretconfig" className={secretconfigtoggle + ' control-panel'}>
					Tags:
					<textarea id="search-tags" defaultValue={searchterms}></textarea>
					Playlist: 
					<input id="playlist-input" type="text" defaultValue={playlist} />
					<button onClick={this.updateSettings}>Save Settings</button>
					<button onClick={this.getState}>Show State</button>
					<button onClick={this.overrideInactivityPause}>Override Inactivity Pause</button>
					<button onClick={this.pauseGifs}>Pause Gif Rotation</button>
					<button onClick={this.playGifs}>Play Gif Rotation</button>
					<button onClick={this.playAudio}>Play Audio</button>
					<button onClick={this.pauseAudio}>Pause Audio</button>
				</div>

			</div>
		)
	},

	bindKeyPresses: function(){
		var self = this;
		$(document).on('keyup', function(e){
			if(!$(e.target).parents('.control-panel').length){
				switch(e.keyCode){
					case KEY_CONST.SPACE:
						self.toggleGlobalPlayPause();
						break;
					case KEY_CONST.ESC:
						self.exitFullscreen();
						break;
					case KEY_CONST.RIGHT_ARROW:
						self.playNextAudioTrack();
					default:
						break;
				}				
			}
		});
	},
	bindMouseEvents: function(){
		var self = this;
		$(document).on('mousemove', function(e){
			self.showPanel();
			clearTimeout(mouseHidePanelTimer);
			//Disable the timer to hide if the mouse is still over the #panel
			if(!$(e.target).parents('.control-panel').length) {
				mouseHidePanelTimer = setTimeout(function(){
					self.hidePanel();
				}, mouseWaitTimeToHidePanel);				
			}
		})
		.on('mouseenter', '.control-panel', function(){
			clearTimeout(mouseHidePanelTimer);
		})
		.on('mouseleave', '.control-panel', function(){
			mouseHidePanelTimer = setTimeout(function(){
				self.hidePanel();
			}, mouseWaitTimeToHidePanel);
		})
		.on('dblclick', function(e){
			if(!$(e.target).parents('.control-panel').length) {
				self.toggleFullscreen();
			}
		})
	},

	//Mobile specific code
	bindTouchEvents: function(){
		var self = this;
		$(document).on('touchstart', function(e){
			if(!$(e.target).parents('.control-panel').length)  {
				if(self.state.showPanel) {
					self.hidePanel();
				}
				else {
					self.showPanel();
				}
			}
		});
	},
	//Check if current device is a mobile device
	isMobileDevice: function(){
		if('ontouchstart' in window /*works on most browsers*/ || navigator.maxTouchPoints /*works on IE10/11 and Surface */){
			return true;
		}
		return false;
	},
	//Toggles the first play for mobile devices
	mobileFirstPlayStart: function(){
		this.playAudio();
		this.setState({
			mobileHasAudioStarted: true
		});
	},

	//Console logs the state
	getState: function(e){
		console.log(this.state);
	},

	//Shuffles a given array
	//Thanks Fisher Yates! http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
	shuffleArray: function(arr){
		var currentIndex = arr.length;
		var temp;
		var randomIndex;

		while( 0 !== currentIndex ){
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex--;

			temp = arr[currentIndex];
			arr[currentIndex] = arr[randomIndex];
			arr[randomIndex] = temp;
		}
		return arr;
	},

	//Toggles the HTML5 full screen
	toggleFullscreen: function(){
		var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
		if(fullscreenElement){
			this.exitFullscreen();
		}
		else {
			this.launchFullscreen();
		}
	},
	//Enabled HTML5 full screen on the document
	launchFullscreen: function(){
		//Lifted straight from https://davidwalsh.name/fullscreen
		var element = document.documentElement;
		if(element.requestFullscreen) {
			element.requestFullscreen();
		} else if(element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if(element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if(element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	},
	//Exits HTML5 full screen
	exitFullscreen: function(){
		//Lifted straight from https://davidwalsh.name/fullscreen
		if( document.exitFullscreen) {
			document.exitFullscreen();
		} else if(document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if(document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	},

	//Binds a protective error for all videos to use the fallback url if a video couldn't be loaded
	bindGifsFallback: function(){
		$('.player-gif-container video').on('error', function(e){
			this.src = fallbackVideoUrl;
		});
	},
	//Bind the window to pause all videos from playing and loading if tab is inactive
	//http://stackoverflow.com/questions/1760250/how-to-tell-if-browser-tab-is-active
	//TODO: throw up a pause screen (tap here to continue playing) in cause user tapped on browser url window
	bindGifsPauseForInactiveWindow: function(){
		var self = this;

		$(window).on('blur focus', function(e) {
			var prevType = $(this).data('prevType');

			if(!self.state.overrideInactivityPause){//ignore everything if we override the pausing
				if (prevType != e.type) {//  reduce double fire issues
					switch (e.type) {
						case 'blur':
							console.log('Gifs paused for window inactivity');
							self.pauseGifs();
							break;
						case 'focus':
							if(self.state.currentlyPlaying) {
								console.log('Gifs resuming');
								self.playGifs();			
							}
							break;
					}
				}
				$(this).data('prevType', e.type);
			}
		})

	},
	//Build the Giphy Search String
	getGifUrlSearchString: function(){
		var self = this;
		var rand = Math.floor(Math.random() * (this.state.gifSearchTags.length));
		var randomTerm = this.state.gifSearchTags[rand];
		return 'http://api.giphy.com/v1/gifs/random?tag=' +  randomTerm + '&api_key=' + giphyApiKey + '&rating=' + giphyRating;
	},
	//Load the Gif from Giphy's Search
	loadGif: function(gifNum){
		var self = this;
		var getUrl = this.getGifUrlSearchString();
		$.ajax(getUrl).done(function(data){
			if(gifNum === 1){
				self.setState({
					gif1Url: data.data.image_mp4_url,
					gif1Meta: data.data.url,
					gif1Img: data.data.image_original_url
				});

			}
			else if (gifNum === 2){
				self.setState({
					gif2Url : data.data.image_mp4_url,
					gif2Meta: data.data.url,
					gif2Img: data.data.image_original_url
				});
			}
		});
	},
	//Manages the rotation of the gifs
	rotateGifs: function(){
		var self = this;
		var nextRotatorState = this.state.rotatorState === 0 ? 1 : 0; //inverses the rotator state

		switch(this.state.rotatorState){
			case 0:
				//Hide the div first, then attempt to load the gif in the background when it's hidden
				self.setState({
					gif1Visible: false
				});
				setTimeout(function(){
					self.loadGif(1);
				}, gifTransitionAnimTime);
			break;
			case 1:
				self.setState({
					gif1Visible: true
				});
				setTimeout(function(){
					self.loadGif(2);
				}, gifTransitionAnimTime);
			break;
		}

		this.setState({
			rotatorState: nextRotatorState
		});

		rotator = setTimeout(function(){
			self.rotateGifs();
		}, rotationTimer );
	},
	//Plays the gifs and begins rotation
	playGifs: function(){
		var self = this;
		this.setState({
			gifCurrentlyPlaying: true
		}, function(){
			clearTimeout(rotator);
			self.rotateGifs();
			$('.player-gif-container video').each(function(i, vid){
				vid.play();
			});
		});
	},
	//Pauses the gifs and stops rotation
	pauseGifs:function(){
		var self = this;
		this.setState({
			gifCurrentlyPlaying: false 
		}, function(){
			$('.player-gif-container video').each(function(i, vid){
				vid.pause();
			});
			clearTimeout(rotator);
		});
	},
	//Outputs the gif permalink
	setGifCredit: function(){
		if(this.state.rotatorState == 0) {
			console.log('Gif 2 Url: ', this.state.gif2Meta)
		}
		else if(this.state.rotatorState == 1){
			console.log('Gif 1 Url: ', this.state.gif1Meta)
		}
	},

	//Initializes the audio player
	initAudioPlayer: function(){
		var self = this;

		//For playlists it's possible to switch to another track in queue
		//e.g. we do it here when playing track is finished 
		scPlayer.on('ended', function () {
			self.playNextAudioTrack();
		});

		//If it throws an error. I'm not 100% sure if this actually works...
		scPlayer.on('error', function () {
			self.playNextAudioTrack();
		});

		//Set that the player has been init
		self.setState({
			playerInit: true
		});
	},
	//Builds an Audio Player based on the playlistUrl
	createAudioPlayer: function(){
		var self = this;
		scPlayer.resolve(this.state.playlistUrl, function (playlist, err) {
			scPlaylist = playlist;

			//Randomize the playlist
			self.shuffleArray(scPlaylist.tracks);

			//Once playlist is loaded it can be played
			self.playNextAudioTrack();

			//Stuff to do first time player is created
			if(!self.state.playerInit){
				self.initAudioPlayer();
			}
		});
	},

	//Plays the current audio track
	playAudio: function(){
		console.log('Audio is playing');
		scPlayer.audio.play();
	},
	//Pauses the current audio track
	pauseAudio: function(){
		console.log('Audio is paused');
		scPlayer.audio.pause();
	},
	//Toggles the current audio track's mute (will keep playing)
	toggleMute: function(){
		if( this.state.currentlyMuted ){
			console.log('Audio is unmuted');
			scPlayer.audio.muted = false;
			this.setState({currentlyMuted : false});
		}
		else {
			console.log('Audio is muted');
			scPlayer.audio.muted = true;
			this.setState({currentlyMuted : true});
		}
	},
	//Skips the player to the next track and loops if it's at the end
	playNextAudioTrack: function(){
		console.log('Playing next track');
		var self = this;
		var tracknum = this.state.currentTrackNum;

		if(isNaN(tracknum) || ( tracknum >= scPlaylist.tracks.length - 1) ) {
			tracknum = 0;
		}
		else {
			tracknum = tracknum + 1;
		}

		//Sets the current track number and sets it to playing with a callback to then play the scPlayer w/track number
		this.setState({
				currentTrackNum : tracknum,
				currentlyPlaying : true
			},
			function(){
				scPlayer.play({playlistIndex: self.state.currentTrackNum});

				//Mobile-specific hack to force user to click screen to play on the first play...
				if(self.isMobileDevice() && self.state.mobileFirstPlay){
					scPlayer.pause();
					self.setState({
						currentlyPlaying : false
					});
				}
			}
		);
	},
	//Toggles the play/pause state for both gifs and audio
	toggleGlobalPlayPause: function(){
		if(!scPlayer.audio.paused){
			this.pauseAudio();
			this.pauseGifs();
			this.setState({
				currentlyPlaying: false
			})
		}
		else {
			this.playAudio();
			this.playGifs();
			this.setState({
				currentlyPlaying: true
			})
		}
	},
	//Display the control panel
	showPanel: function(){
		this.setState({ showPanel: true });
	},
	//Hide the control panel
	hidePanel: function(){
		this.setState({	showPanel: false });
	},

	//Displays the advanced control panel
	toggleSecretConfig: function(){
		if(this.state.showSecretConfig){
			this.setState({
				showSecretConfig: false
			});
		}
		else {
			this.setState({
				showSecretConfig: true
			});			
		}
	},
	//Used for debugging to override window inactvity pausing gifs
	overrideInactivityPause: function(){
		this.setState({
			overrideInactivityPause: true
		});
	},
	//Users the control panel's data and updates the state
	updateSettings: function(){
		var self = this;
		var newGifTags = $('#search-tags').val().trim().split(',');
		var newPlaylist = $('#playlist-input').val().trim();
		var currentPlaylist = this.state.playlistUrl;

		//Giphy tags
		if(newGifTags.length){
			console.log('New Tags: ', newGifTags);
			this.setState({
				gifSearchTags : newGifTags
			});			
		}

		//Playlist Change
		if(newPlaylist && newPlaylist !== currentPlaylist){
			console.log('Launching new player');
			this.setState({
				playlistUrl : newPlaylist
			}, this.createAudioPlayer );
		}
	}
});

ReactDOM.render(<App />, document.getElementById('app'));