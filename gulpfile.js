var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var livereload = require('gulp-livereload');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var reactify = require('reactify');
var cssify = require('cssify');
var uglify = require('gulp-uglify');
var stripDebug = require('gulp-strip-debug');

/* HTML file copy */
gulp.task('html', function(){
	return gulp.src('src/*.html')
	.pipe(gulp.dest('build'));
});

/* Asset folder copy */
gulp.task('copy-assets', function(){
	return (gulp.src('src/assets/**/*'))
	.pipe(gulp.dest('./build/assets/'))
})

/* Browserify React JSX transformation and packaging */
gulp.task('build-react', function(){
	return browserify({
		entries: ['./src/app.jsx'],
		transform: ['cssify', 'reactify']
	})
	.on('error', function(err){
		console.log(err.message);
	})
	.bundle()
	.pipe(source('app.js'))
	.pipe(gulp.dest('./build/'));
});

/* Production version of build-react */
gulp.task('build-react-prod', function(){
	return browserify({
		entries: ['./src/app.jsx'],
		transform: ['cssify', 'reactify']
	})
	.on('error', function(err){
		console.log(err.message);
	})
	.bundle()
	.pipe(source('app.js'))
	//Vinyl buffer required to change from stream to buffered vinyl object. See:http://stackoverflow.com/questions/24992980/how-to-uglify-output-with-browserify-in-gulp
	//See : http://justinjohnson.org/javascript/getting-started-with-gulp-and-browserify/#intro-to-gulp
	//and also: https://blog.engineyard.com/2015/client-side-javascript-project-gulp-and-browserify
	.pipe(buffer())
	.pipe(stripDebug())
	.pipe(uglify({
		preserveComments: false,
	}))
	.pipe(gulp.dest('./build/'));
});

/* Gulp test task */
gulp.task('default', function() {
  // place code for your default task here
  console.log('gulp working, good job.');
});

/* Regular build task */
gulp.task('build', ['html', 'build-react', 'copy-assets']);

/* Prod build task */
gulp.task('build-prod', ['html', 'build-react-prod', 'copy-assets']);

/* Live reload watch build task */
gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('build/**').on('change', livereload.changed);

	//Run default build tasks on changes in src 
	watch('src/**', function(){
		gulp.start('build');
	});
})